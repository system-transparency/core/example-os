# example-os

Kernel and initramfs for building the [demo os package](https://git.glasklar.is/system-transparency/core/system-transparency).

Latest: [ubuntu-focal v1](https://git.glasklar.is/system-transparency/core/example-os/-/packages/3)
